<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 20/01/16
 * Time: 8:09 PM
 */

$second_query = array(
		'post_type' => 'property',
		'tax_query' => $tax_query,
		'posts_per_page' => $similer_count,
		'orderby' => 'rand'
	);

	$wp_query = new WP_Query($second_query);
?>
<div class="item-wrap"><h4 class="not-found"><?php esc_html_e('Sorry No Results Found', 'houzez') ?></h4><br />Te pueden interesar otras propiedades.
<div class="property-listing <?php echo esc_attr($similer_view); ?>">
					<div class="row">

						<?php
						while ($wp_query->have_posts()) : $wp_query->the_post();

							get_template_part('template-parts/property-for-listing');

						endwhile;
						?>

					</div>
				</div></div>
