<?php
require __DIR__.'/vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__.'/../../../backend');
$dotenv->load();

function my_theme_enqueue_styles()
{
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style($parent_style, get_template_directory_uri().'/style.css');
    wp_enqueue_style('child-style',
        get_stylesheet_directory_uri().'/style.css',
        array($parent_style),
        wp_get_theme()->get('Version')
        );
    require_once get_stylesheet_directory().'/framework/options/fave-options.php';

    wp_enqueue_style('sweetalert.min', get_stylesheet_directory_uri().'/assets/plugins/sweet-alert/sweetalert.min.css', array(), '1.1.3', 'all');
    wp_enqueue_script('sweetalert.min', get_stylesheet_directory_uri().'/assets/plugins/sweet-alert/sweetalert.min.js', array(), '1.1.3', 'all');

    wp_dequeue_script('houzez_ajax_calls');
    wp_enqueue_script('houzez_ajax_calls', get_stylesheet_directory_uri().'/js/houzez_ajax_calls.js', array('jquery'), HOUZEZ_THEME_VERSION, true);
    wp_localize_script('houzez_ajax_calls', 'HOUZEZ_ajaxcalls_vars',
        array(
            'admin_url' => get_admin_url(),
            'houzez_rtl' => $houzez_rtl,
            'redirect_type' => $after_login_redirect,
            'login_redirect' => $login_redirect,
            'login_loading' => esc_html__('Sending user info, please wait...', 'houzez'),
            'direct_pay_text' => esc_html__('Processing, Please wait...', 'houzez'),
            'user_id' => $userID,
            'transparent_menu' => $fave_main_menu_trans,
            'simple_logo' => $simple_logo,
            'retina_logo' => $retina_logo_url,
            'retina_logo_mobile' => $retina_mobilelogo_url,
            'retina_logo_mobile_splash' => $retina_logo_mobile_splash,
            'retina_logo_splash' => $retina_splash_logo_url,
            'retina_logo_height' => $retina_logo_height,
            'retina_logo_width' => $retina_logo_width,
            'property_lat' => $property_lat,
            'property_lng' => $property_lng,
            'property_map' => $property_map,
            'property_map_street' => $property_streetView,
            'is_singular_property' => $is_singular_property,
            'process_loader_refresh' => 'fa fa-spin fa-refresh',
            'process_loader_spinner' => 'fa fa-spin fa-spinner',
            'process_loader_circle' => 'fa fa-spin fa-circle-o-notch',
            'process_loader_cog' => 'fa fa-spin fa-cog',
            'success_icon' => 'fa fa-check',
            'prop_featured' => esc_html__('Featured', 'houzez'),
            'featured_listings_none' => esc_html__('You have used all the "Featured" listings in your package.', 'houzez'),
            'prop_sent_for_approval' => esc_html__('Sent for Approval', 'houzez'),
            'paypal_connecting' => esc_html__('Connecting to paypal, Please wait... ', 'houzez'),
            'confirm' => esc_html__('Are you sure you want to delete?', 'houzez'),
            'confirm_featured' => esc_html__('Are you sure you want to make this a featured listing?', 'houzez'),
            'confirm_relist' => esc_html__('Are you sure you want to relist this property?', 'houzez'),
            'not_found' => esc_html__("We didn't find any results", 'houzez'),
            'for_rent' => $advanced_search_rent_status,
            'for_rent_price_range' => $advanced_search_price_range_rent_status,
            'currency_symbol' => $currency_symbol,
            'advanced_search_widget_min_price' => $advanced_search_widget_min_price,
            'advanced_search_widget_max_price' => $advanced_search_widget_max_price,
            'advanced_search_min_price_range_for_rent' => $advanced_search_min_price_range_for_rent,
            'advanced_search_max_price_range_for_rent' => $advanced_search_max_price_range_for_rent,
            'advanced_search_widget_min_area' => $advanced_search_widget_min_area,
            'advanced_search_widget_max_area' => $advanced_search_widget_max_area,
            'advanced_search_price_slide' => houzez_option('adv_search_price_slider'),
            'fave_page_template' => basename(get_page_template()),
            'google_map_style' => houzez_option('googlemap_stype'),
            'googlemap_default_zoom' => $googlemap_zoom_level,
            'googlemap_pin_cluster' => $googlemap_pin_cluster,
            'googlemap_zoom_cluster' => $googlemap_zoom_cluster,
            'map_icons_path' => get_template_directory_uri().'/images/map/',
            'infoboxClose' => get_template_directory_uri().'/images/map/close.png',
            'clusterIcon' => get_template_directory_uri().'/images/map/cluster-icon.png',
            'google_map_needed' => $google_map_needed,
            'page' => $paged,
            'search_result_page' => $search_result_page,
            'search_keyword' => $search_keyword,
            'search_country' => $search_country,
            'search_state' => $search_state,
            'search_city' => $search_city,
            'search_feature' => $search_feature,
            'search_area' => $search_area,
            'search_status' => $search_status,
            'search_type' => $search_type,
            'search_bedrooms' => $search_bedrooms,
            'search_bathrooms' => $search_bathrooms,
            'search_min_price' => $search_min_price,
            'search_max_price' => $search_max_price,
            'search_min_area' => $search_min_area,
            'search_max_area' => $search_max_area,
            'search_publish_date' => $search_publish_date,

            'search_location' => $search_location,
            'use_radius' => $use_radius,
            'search_lat' => $search_lat,
            'search_long' => $search_long,
            'search_radius' => $search_radius,

            'transportation' => esc_html__('Transportation', 'houzez'),
            'supermarket' => esc_html__('Supermarket', 'houzez'),
            'schools' => esc_html__('Schools', 'houzez'),
            'libraries' => esc_html__('Libraries', 'houzez'),
            'pharmacies' => esc_html__('Pharmacies', 'houzez'),
            'hospitals' => esc_html__('Hospitals', 'houzez'),
            'sort_by' => $sort_by,
            'measurement_updating_msg' => esc_html__('Updating, Please wait...', 'houzez'),
            'currency_updating_msg' => esc_html__('Updating Currency, Please wait...', 'houzez'),
            'currency_position' => houzez_option('currency_position'),
            'submission_currency' => houzez_option('currency_paid_submission'),
            'wire_transfer_text' => esc_html__('To be paid', 'houzez'),
            'direct_pay_thanks' => esc_html__('Thank you. Please check your email for payment instructions.', 'houzez'),
            'direct_payment_title' => esc_html__('Direct Payment Instructions', 'houzez'),
            'direct_payment_button' => esc_html__('SEND ME THE INVOICE', 'houzez'),
            'direct_payment_details' => houzez_option('direct_payment_instruction'),
            'measurement_unit' => $measurement_unit_adv_search,
            'header_map_selected_city' => $header_map_selected_city,
            'thousands_separator' => $thousands_separator,
            'current_tempalte' => $current_template,
            'monthly_payment' => esc_html__('Monthly Payment', 'houzez'),
            'weekly_payment' => esc_html__('Weekly Payment', 'houzez'),
            'bi_weekly_payment' => esc_html__('Bi-Weekly Payment', 'houzez'),
            'compare_button_url' => houzez_get_template_link_2('template/template-compare.php'),
            'template_thankyou' => houzez_get_template_link('template/template-thankyou.php'),
            'compare_page_not_found' => esc_html__('Please create page using compare properties template', 'houzez'),
            'property_detail_top' => esc_attr($property_top_area),
            'houzez_autoComplete' => houzez_autocomplete_search(),
            'keyword_search_field' => $keyword_field,
            'keyword_autocomplete' => $keyword_autocomplete,
            'houzez_date_language' => $houzez_date_language,
            'houzez_default_radius' => $houzez_default_radius,
            'enable_radius_search' => $enable_radius_search,
            'enable_radius_search_halfmap' => $enable_radius_search_halfmap,
            'houzez_primary_color' => $houzez_primary_color,
            'geocomplete_country' => $geocomplete_country,
            'houzez_logged_in' => $houzez_logged_in,
            'ipinfo_location' => houzez_option('ipinfo_location'),
            )
    ); // end ajax calls
}

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

// Load translation files from your child theme instead of the parent theme
function my_child_theme_locale()
{
    load_child_theme_textdomain('houzez', get_stylesheet_directory().'/languages');
}

add_action('after_setup_theme', 'my_child_theme_locale');

//WP ALL Import plugin - para que funcione con las galerías

add_action('pmxi_gallery_image', 'my_gallery_image', 10, 3);
function my_gallery_image($pid, $attid, $image_filepath) {
   add_post_meta($pid, 'fave_property_images', $attid);
}

function custom_jetpack_default_image() {
    return 'https://inmueblespetrela.com/wp-content/uploads/2017/03/slider2.jpg';
}
add_filter( 'jetpack_open_graph_image_default', 'custom_jetpack_default_image' );


?>