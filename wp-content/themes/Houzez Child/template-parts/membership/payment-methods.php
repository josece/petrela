<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 09/09/16
 * Time: 12:56 PM
 */
global $current_user;

$current_user = wp_get_current_user();
$userID = $current_user->ID;

$selected_package_id = isset($_GET['selected_package']) ? $_GET['selected_package'] : '';
$pack_price = get_post_meta($selected_package_id, 'fave_package_price', true);
$pack_title = get_the_title($selected_package_id);
$terms_conditions = houzez_option('payment_terms_condition');
$allowed_html_array = array(
    'a' => array(
        'href' => array(),
        'title' => array(),
    ),
);
$enable_paypal = houzez_option('enable_paypal');
$enable_stripe = houzez_option('enable_stripe');
$enable_conekta = 1; // houzez_option('enable_conekta');
$enable_wireTransfer = houzez_option('enable_wireTransfer');

if ($pack_price > 0) {
    ?>
    <div class="method-select-block">

        <?php if ($enable_paypal != 0) {?>
        <div class="method-row">
            <div class="method-select">
                <div class="radio">
                    <label>
                        <input type="radio" class="payment-paypal" name="houzez_payment_type" value="paypal" checked>
                        <?php esc_html_e('Paypal', 'houzez');?>
                    </label>
                </div>
            </div>
            <div class="method-type"><img src="<?php echo get_template_directory_uri(); ?>/images/paypal-icon.jpg" alt="paypal"></div>
        </div>
        <div class="method-option">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="paypal_package_recurring" id="paypal_package_recurring" value="1">
                    <?php esc_html_e('Set as recurring payment', 'houzez');?>
                </label>
            </div>
        </div>
        <?php }?>

        <?php if ($enable_stripe != 0) {?>
        <div class="method-row">
            <div class="method-select">
                <div class="radio">
                    <label>
                        <input type="radio" class="payment-stripe" name="houzez_payment_type" value="stripe">
                        <?php esc_html_e('Stripe', 'houzez');?>
                    </label>
                    <?php houzez_stripe_payment_membership($selected_package_id, $pack_price, $pack_title);?>
                </div>
            </div>
            <div class="method-type"><img src="<?php echo get_template_directory_uri(); ?>/images/stripe-icon.jpg" alt="stripe"></div>
        </div>
        <div class="method-option">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="stripe_package_recurring" id="stripe_package_recurring" value="1">
                    <?php esc_html_e('Set as recurring payment', 'houzez');?>
                </label>
            </div>
        </div>
        <?php }?>

  <?php if ($enable_conekta != 0): ?>
<!--     </div>
</form>
<div class="method-select-block"> -->
    <div class="method-row">
        <div class="method-select">
            <div class="radio">
                <label>
                    <input type="radio" class="payment-conekta" name="houzez_payment_type" value="conekta-card" checked="checked">
                    <?php esc_html_e('Tarjeta de crédito/débito', 'houzez');?>
                </label>
                <?php // houzez_conekta_payment_membership($selected_package_id, $pack_price, $pack_title);?>
            </div>
        </div>
        <div class="method-type"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tarjetas.png" alt="Conekta"  style="height:40px"></div>
    </div>
    <div class="method-option" style="display: block; overflow: auto">
        <div class="payment_box payment_method_conektacard" style="display: block;">
            <div class="clear"></div>
            <span style="width: 100%; float: left; color: red;" class="payment-errors required"></span>
            <div class="form-row form-row-wide woocommerce-validated">
              <label for="conekta-card-number">Número de la tarjeta<span class="required">*</span></label>
              <input id="conekta-card-number" class="input-text  form-control" type="text" data-conekta="card[number]">
          </div>

          <div class="form-row form-row-wide">
              <label for="conekta-card-name"> Nombre del tarjetahabiente<span class="required">*</span></label>
              <input id="conekta-card-name" type="text" data-conekta="card[name]" class="input-text form-control">
          </div>

          <div class="clear"></div>

          <p class="form-row form-row-first">
          <div class="col-xs-4">
            <label for="card_expiration">Mes de expiración <span class="required">*</span></label>
            <select id="card_expiration" data-conekta="card[exp_month]" class="month  form-control" autocomplete="off" required="required">
               <option selected="selected" value="">Mes</option>
               <option value="1">01 - Enero</option>
               <option value="2">02 - Febrero</option>
               <option value="3">03 - Marzo</option>
               <option value="4">04 - Abril</option>
               <option value="5">05 - Mayo</option>
               <option value="6">06 - Junio</option>
               <option value="7">07 - Julio</option>
               <option value="8">08 - Agosto</option>
               <option value="9">09 - Septiembre</option>
               <option value="10">10 - Octubre</option>
               <option value="11">11 - Noviembre</option>
               <option value="12">12 - Diciembre</option>
           </select>
           </div>
           <div class="col-xs-4">
        <label>Año de expiración<span class="required">*</span></label>
        <select id="card_expiration_yr" data-conekta="card[exp_year]" class="year  form-control" autocomplete="off">
          <option selected="selected" value=""> Año</option>
          <option value="2016">2016</option>
          <option value="2017">2017</option>
          <option value="2018">2018</option>
          <option value="2019">2019</option>
          <option value="2020">2020</option>
          <option value="2021">2021</option>
          <option value="2022">2022</option>
          <option value="2023">2023</option>
          <option value="2024">2024</option>
          <option value="2025">2025</option>
          <option value="2026">2026</option>
      </select>
      </div>
      <div class="col-xs-4">
    <label for="conekta-card-cvc">CVC <span class="required">*</span></label>
    <input id="conekta-card-cvc" class="input-text  form-control" type="text" maxlength="4" data-conekta="card[cvc]" value="" style="border-radius:6px" required="required">
    </div>
</p>

<div class="clear"></div>
</div>
</div>
<div class="method-row">
    <div class="method-select">
        <div class="radio">
            <label>
                <input type="radio" class="payment-conekta" name="houzez_payment_type" value="oxxo">
                <?php esc_html_e('OXXO', 'houzez');?>
            </label>
            <?php // houzez_conekta_payment_membership($selected_package_id, $pack_price, $pack_title);?>
        </div>
    </div>
    <div class="method-type"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/oxxo.png" alt="OXXO" style="height:40px"></div>
</div>
<div class="method-option">
    <div class="checkbox">
        <label>
            Por favor realiza el pago en el OXXO más cercano utilizando la ficha de pago. Dale clic para generar la ficha de pago con codigo de barra.
        </label>
    </div>
</div>
<?php endif?>
<?php if ($enable_wireTransfer != 0) {?>
<div class="method-row">
    <div class="method-select">
        <div class="radio">
            <label>
                <input type="radio" name="houzez_payment_type" value="direct_pay">
                <?php esc_html_e('Direct Bank Transfer', 'houzez');?>
            </label>
        </div>
    </div>
    <div class="method-type method-description">
        <p> <?php esc_html_e('Make your payment direct into your bank account. Please use order ID as the payment reference', 'houzez');?> </p>
    </div>
</div>
<?php }?>

</div>
<?php }?>

<input type="hidden" name="houzez_package_id" value="<?php echo esc_attr($selected_package_id); ?>">
<input type="hidden" name="houzez_package_price" value="<?php echo esc_attr($pack_price); ?>">
<?php if ($pack_price > 0) {?>
<button id="houzez_complete_membership" type="submit" class="btn btn-success btn-submit"> <?php esc_html_e('Completar Membresía', 'houzez');?> </button>
<span class="help-block"><?php echo sprintf(wp_kses(__('Al dar clic en "Completar Membresía", está aceptando nuestros <a href="%s">Términos y condiciones</a>', 'houzez'), $allowed_html_array), get_permalink($terms_conditions)); ?></span>
<?php } else {
    if (is_user_logged_in()) {
        if (houzez_user_had_free_package($userID)) {?>

        <button id="houzez_complete_membership" type="submit"
        class="btn btn-success btn-submit"> <?php esc_html_e('Completar Membresía', 'houzez');?> </button>
        <span
        class="help-block"><?php echo sprintf(wp_kses(__('Al dar clic en "Completar Membresía", está aceptando nuestros <a href="%s">Términos y condiciones</a>', 'houzez'), $allowed_html_array), get_permalink($terms_conditions)); ?></span>

        <?php } else {?>
        <span
        class="help-block free-membership-used"><?php esc_html_e('Ya has utilizado el plan gratuito, por favor selecciona uno distinto.', 'houzez');?></span>
        <?php
}
    } else {?>
<button id="houzez_complete_membership" type="submit"
class="btn btn-success btn-submit"> <?php esc_html_e('Completar Membresía', 'houzez');?> </button>
<span
class="help-block"><?php echo sprintf(wp_kses(__('Al dar clic en "Completar Membresía", está aceptando nuestros <a href="%s">Términos y condiciones</a>', 'houzez'), $allowed_html_array), get_permalink($terms_conditions)); ?></span>
<?php
}
}?>
