<?php
/**
 * Template Name: Conekta (OXXO)
 * Created by PhpStorm.
 * User: Jose Calleja
 * Date: 11/09/16
 * Time: 3:30 PM
 */
/* -----------------------------------------------------------------------------------------------------------
 *  Process Messages from Conekta
 *
 * Reference Links:
 * https://www.conekta.io/es/docs/tutorial/pagos-con-tarjeta?vertical=productos-digitales&backend-language=php
-------------------------------------------------------------------------------------------------------------*/
header('Content-Type: application/json');

if (!isset($_POST['package_id']) || !is_numeric($_POST['package_id'])) {
    die();
}

if (!isset($_POST['payment_gateway'])) {
    die();
}

class AppClient extends \GuzzleHttp\Client
{
    protected $client;

    public function __construct()
    {
        parent::__construct([
            'base_uri' => "http://backend.inmueblespetrela.com/api/v1/",
            'verify' => false,
            'timeout' => 3600,
            'query' => wp_get_current_user()->ID != 0 ? [
                'key' => getenv('PETRELA_API_KEY'),
            ] : [],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
    }
}

$client = new AppClient;

$pack_id = $packageId = $_POST['package_id'];
$paymentGateway = $_POST['payment_gateway'];
$conektaTokenId = isset($_POST['conekta_token_id']) ? $_POST['conekta_token_id'] : null;
$current_user = wp_get_current_user();
$userID = $current_user->ID;
$user_email = $current_user->user_email;
$admin_email = get_bloginfo('admin_email');
$username = $current_user->user_login;
$thankyou_page_link = houzez_get_template_link('template/template-thankyou.php');
$time = time();
$date = date('Y-m-d H:i:s', $time);

if ($paymentGateway == 'conekta-card') {
    $paymentMethod = 'conekta-card';

    $invoiceID = houzez_generate_invoice('package', 'one_time', $pack_id, $date, $userID, 0, 0, '', $paymentMethod);
    // set as unpaid
    update_post_meta($invoiceID, 'invoice_payment_status', 0);

    try {
        $response = $client->post("payments/card/process", [
            'form_params' => [
                'package_id' => $packageId,
                'card_token' => $conektaTokenId,
                'user_id' => $userID,
                'invoice_id' => $invoiceID,
            ],
        ]);

        if ($response->getStatusCode() !== 200) {
            die;
        }

        houzez_save_user_packages_record($userID);
        if (houzez_check_user_existing_package_status($current_user->ID, $pack_id)) {
            houzez_downgrade_package($current_user->ID, $pack_id);
            houzez_update_membership_package($userID, $pack_id);
        } else {
            houzez_update_membership_package($userID, $pack_id);
        }

        // it's ok, set as paid.
        update_post_meta($invoiceID, 'invoice_payment_status', 1);
        // $paymentMethod = json_decode($response->getBody());

        echo json_encode($thankyou_page_link);
        die();
    } catch (\Exception $e) {
        header('HTTP/1.1 402 Payment Required');
        die;
    }
} else if ($paymentGateway == 'oxxo') {
    $paymentMethod = 'conekta-oxxo';

    $invoiceID = houzez_generate_invoice('package', 'one_time', $pack_id, $date, $userID, 0, 0, '', $paymentMethod);
    update_post_meta($invoiceID, 'invoice_payment_status', 0);

    try {
        $response = $client->post("payments/oxxo/process", [
            'form_params' => [
                'package_id' => $packageId,
                'user_id' => $userID,
                'invoice_id' => $invoiceID,
            ],
        ]);

        if ($response->getStatusCode() !== 200) {
            die;
        }

        $paymentMethod = json_decode($response->getBody());

        $thankyou_page_link_params = http_build_query([
            'barcode' => $paymentMethod->barcode,
            'expires_at' => $paymentMethod->expiresAt,
            'store_name' => $paymentMethod->storeName,
            'package_id' => $packageId,
        ]);

        $thankyou_page_link .= (strpos($thankyou_page_link, '?') ? '&' : '?').$thankyou_page_link_params;

        echo json_encode($thankyou_page_link);
        die;
    } catch (\Exception $e) {
        header('HTTP/1.1 402 Payment Required');
        die;
    }
} else if ($paymentGateway == getenv('PETRELA_API_KEY')) {
    if (!isset($_POST['user_id'])) {
        die();
    }
    if (!isset($_POST['package_id'])) {
        die();
    }
    if (!isset($_POST['invoice_id'])) {
        die();
    }

    $userID = $_POST['user_id'];
    $packID = $_POST['package_id'];
    $invoiceID = $_POST['invoice_id'];

    houzez_save_user_packages_record($userID);
    if (houzez_check_user_existing_package_status($userID, $packID)) {
        houzez_downgrade_package($userID, $packID);
        houzez_update_membership_package($userID, $packID);
    } else {
        houzez_update_membership_package($userID, $packID);
    }

    update_post_meta($invoiceID, 'invoice_payment_status', 1);

    header('HTTP/1.1 200 Ok');
    die;
}

die;

use Conekta\Conekta;

$allowed_html = array();

$current_user = wp_get_current_user();
$userID = $current_user->ID;
$user_email = $current_user->user_email;
$admin_email = get_bloginfo('admin_email');
$username = $current_user->user_login;
$submission_currency = houzez_option('currency_paid_submission');
$thankyou_page_link = houzez_get_template_link('template/template-thankyou.php');
$paymentMethod = 'Conekta';
$time = time();
$date = date('Y-m-d H:i:s', $time);
/*
$conekta_test_secret_key = houzez_option('conekta_test_secret_key');
$conekta_test_public_key = houzez_option('conekta_test_public_key');
$conekta_live_secret_key = houzez_option('conekta_live_secret_key');
$conekta_live_public_key = houzez_option('conekta_live_public_key');
$is_test = houzez_option('conekta_is_test');
$conekta = array(
"test_secret_key" => $conekta_test_secret_key,
"test_public_key" => $conekta_test_public_key,
"live_secret_key" => $conekta_live_secret_key,
"live_public_key" => $conekta_live_public_key,
);

if (!$is_test) {
Conekta::setApiKey($conekta['test_secret_key']);
} else {
Conekta::setApiKey($conekta['conekta_live_secret_key']);
}*/

// // Retrieve the request's body and parse it as JSON
// $input = @file_get_contents("php://input");

// if( is_email($_POST['stripeEmail']) ) {  // done
//     $stripeEmail=  wp_kses ( esc_html($_POST['stripeEmail']) ,$allowed_html );
// } else {
//     wp_die('None Mail');
// }

// if( isset($_POST['userID']) && !is_numeric( $_POST['userID'] ) ) { //done
//     die();
// }

// if( isset($_POST['propID']) && !is_numeric( $_POST['propID'] ) ) { //done
//     die();
// }

if (isset($_POST['submission_pay']) && !is_numeric($_POST['submission_pay'])) {
    //done
    die();
}

// if( isset($_POST['pack_id']) && !is_numeric( $_POST['pack_id'] ) ){
//     die();
// }

// if( isset($_POST['pay_ammout']) && !is_numeric( $_POST['pay_ammout'] ) ) { //done
//     die();
// }

// if( isset($_POST['featured_pay']) && !is_numeric( $_POST['featured_pay'] ) ){
//     die();
// }

// if( isset($_POST['is_upgrade']) && !is_numeric( $_POST['is_upgrade'] ) ){
//     die();
// }

// if( isset($_POST['houzez_stripe_recurring']) && !is_numeric( $_POST['houzez_stripe_recurring'] ) ){
//     die();
// }

echo "test";
if (isset($_POST['submission_pay']) && $_POST['submission_pay'] == 1) {
    try {

        // $token  = wp_kses ( $_POST['stripeToken'] ,$allowed_html);

        // $customer = \Stripe\Customer::create(array(
        //     "email" => $stripeEmail,
        //     "source" => $token // obtained with Stripe.js
        // ));

        $userId = intval($_POST['userID']);
        $listing_id = intval($_POST['propID']);
        $pay_ammout = intval($_POST['pay_ammout']);
        $is_featured = 0;
        $is_upgrade = 0;

        if (isset($_POST['featured_pay']) && $_POST['featured_pay'] == 1) {
            $is_featured = intval($_POST['featured_pay']);
        }

        if (isset($_POST['is_upgrade']) && $_POST['is_upgrade'] == 1) {
            $is_upgrade = intval($_POST['is_upgrade']);
        }

        // $charge = \Stripe\Charge::create(array(
        //     "amount" => $pay_ammout,
        //     'customer' => $customer->id,
        //     "currency" => $submission_currency,
        //     //"source" => "tok_18Qks9IwlqUqVdUMkzqkPsbV", // obtained with Stripe.js
        //     //"description" => ""
        // ));
        $charge = \Conekta\Charge::create(array(
            "amount" => $pay_ammout,
            "currency" => "MXN",
            "description" => get_the_title($listing_id),
            'customer' => $customer->id,
            "reference_id" => "$customer->id",
            "card" => $_POST['conektaTokenId'],
            //"tok_a4Ff0dD2xYZZq82d9"
        ));

        // update_post_meta( $listing_id, 'fave_payment_status', 'paid' );

        // $paid_submission_status    = houzez_option('enable_paid_submission');
        // $listings_admin_approved = houzez_option('listings_admin_approved');

        // if( $listings_admin_approved != 'yes'  && $paid_submission_status == 'per_listing' ){
        //     $post = array(
        //         'ID'            => $listing_id,
        //         'post_status'   => 'publish'
        //     );
        //     $post_id =  wp_update_post($post );
        // } else {
        //     $post = array(
        //         'ID'            => $listing_id,
        //         'post_status'   => 'pending'
        //     );
        //     $post_id =  wp_update_post($post );
        // }

        // if( $is_featured == 1 ) {
        //     update_post_meta( $listing_id, 'fave_featured', 1 );
        //     $invoice_id = houzez_generate_invoice( 'Publish Listing with Featured', 'one_time', $listing_id, $date, $userID, 1, 0, '', $paymentMethod );
        // } else {
        //     $invoice_id = houzez_generate_invoice( 'Listing', 'one_time', $listing_id, $date, $userID, 0, 0, '', $paymentMethod );
        // }
        // update_post_meta( $invoice_id, 'invoice_payment_status', 1 );

        // $args = array(
        //     'listing_title'  =>  get_the_title($listing_id),
        //     'listing_id'     =>  $listing_id,
        //     'invoice_no' =>  $invoice_id,
        // );

        /*
         * Send email
         * */
        // houzez_email_type( $user_email, 'paid_submission_listing', $args);
        // houzez_email_type( $admin_email, 'admin_paid_submission_listing', $args);
        // }

        wp_redirect($thankyou_page_link);exit;

    } catch (Conekta_Error $e) {
        //El pago no pudo ser procesado
        $error = '<div class="alert alert-danger">
                <strong>Error!</strong> '.$e->getMessage().'
                </div>';
        print $error;
    }
}
