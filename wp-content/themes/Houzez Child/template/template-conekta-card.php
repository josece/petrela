<?php
/**
 * Template Name: Conekta Card
 * Created by PhpStorm.
 * User: Jose Calleja
 * Date: 11/09/16
 * Time: 3:30 PM
 */
/* -----------------------------------------------------------------------------------------------------------
*  Process Messages from Conekta
 *
 * Reference Links:
 * https://www.conekta.io/es/docs/tutorial/pagos-con-tarjeta?vertical=productos-digitales&backend-language=php
-------------------------------------------------------------------------------------------------------------*/

require_once(get_template_directory() . '/framework/conekta-php/lib/Conekta.php');
$allowed_html =array();

$current_user = wp_get_current_user();
$userID       =   $current_user->ID;
$user_email   =   $current_user->user_email;
$admin_email      =  get_bloginfo('admin_email');
$username     =   $current_user->user_login;
$submission_currency = houzez_option('currency_paid_submission');
$thankyou_page_link = houzez_get_template_link('template/template-thankyou.php');
$paymentMethod = 'Conekta';
$time = time();
$date = date('Y-m-d H:i:s',$time);


$conekta_test_secret_key = houzez_option('conekta_test_secret_key');
$conekta_test_public_key = houzez_option('conekta_test_public_key');
$conekta_live_secret_key = houzez_option('conekta_live_secret_key');
$conekta_live_public_key = houzez_option('conekta_live_public_key');
$is_test = houzez_option('conekta_is_test');
$conekta = array(
    "test_secret_key"      => $conekta_test_secret_key,
    "test_public_key" => $conekta_test_public_key,
    "live_secret_key"      => $conekta_live_secret_key,
    "live_public_key" => $conekta_live_public_key,
);

if(!$is_test)
    \Conekta\Conekta::setApiKey($conekta['test_secret_key']);
else
    \Conekta\Conekta::setApiKey($conekta['conekta_live_secret_key']);

// // Retrieve the request's body and parse it as JSON
// $input = @file_get_contents("php://input");

// if( is_email($_POST['stripeEmail']) ) {  // done
//     $stripeEmail=  wp_kses ( esc_html($_POST['stripeEmail']) ,$allowed_html );
// } else {
//     wp_die('None Mail');
// }

// if( isset($_POST['userID']) && !is_numeric( $_POST['userID'] ) ) { //done
//     die();
// }

// if( isset($_POST['propID']) && !is_numeric( $_POST['propID'] ) ) { //done
//     die();
// }

if( isset($_POST['submission_pay']) && !is_numeric( $_POST['submission_pay'] ) ) { //done
    die();
}

// if( isset($_POST['pack_id']) && !is_numeric( $_POST['pack_id'] ) ){
//     die();
// }

// if( isset($_POST['pay_ammout']) && !is_numeric( $_POST['pay_ammout'] ) ) { //done
//     die();
// }

// if( isset($_POST['featured_pay']) && !is_numeric( $_POST['featured_pay'] ) ){
//     die();
// }

// if( isset($_POST['is_upgrade']) && !is_numeric( $_POST['is_upgrade'] ) ){
//     die();
// }

// if( isset($_POST['houzez_stripe_recurring']) && !is_numeric( $_POST['houzez_stripe_recurring'] ) ){
//     die();
// }


if ( isset ($_POST['submission_pay'])  && $_POST['submission_pay'] == 1  ) {
    try {
        // $token  = wp_kses ( $_POST['stripeToken'] ,$allowed_html);

        // $customer = \Stripe\Customer::create(array(
        //     "email" => $stripeEmail,
        //     "source" => $token // obtained with Stripe.js
        // ));

        $userId      = intval( $_POST['userID'] );
        $listing_id  = intval( $_POST['propID'] );
        $pay_ammout  = intval( $_POST['pay_ammout'] );
        $is_featured = 0;
        $is_upgrade  = 0;

        if ( isset($_POST['featured_pay']) && $_POST['featured_pay']==1 ){
            $is_featured    =   intval($_POST['featured_pay']);
        }

        if ( isset($_POST['is_upgrade']) && $_POST['is_upgrade']==1 ){
            $is_upgrade = intval($_POST['is_upgrade']);
        }

        // $charge = \Stripe\Charge::create(array(
        //     "amount" => $pay_ammout,
        //     'customer' => $customer->id,
        //     "currency" => $submission_currency,
        //     //"source" => "tok_18Qks9IwlqUqVdUMkzqkPsbV", // obtained with Stripe.js
        //     //"description" => ""
        // ));
        $charge = \Conekta\Charge::create(array(
            "amount"=> $pay_ammout,
            "currency"=> "MXN",
            "description"=> get_the_title($listing_id),
            'customer' => $customer->id,
            "reference_id"=> "$customer->id",
            "card"=> $_POST['conektaTokenId']
            //"tok_a4Ff0dD2xYZZq82d9"
                    ));
            

        if( $is_upgrade == 1 ) {
            update_post_meta( $listing_id, 'fave_featured', 1 );
            $invoice_id = houzez_generate_invoice( 'Upgrade to Featured', 'one_time', $listing_id, $date, $userID, 0, 1, '', $paymentMethod );
            update_post_meta( $invoice_id, 'invoice_payment_status', 1 );

            $args = array(
                'listing_title'  =>  get_the_title($listing_id),
                'listing_id'     =>  $listing_id,
                'invoice_no' =>  $invoice_id,
            );

            /*
             * Send email
             * */
            houzez_email_type( $user_email, 'featured_submission_listing', $args);
            houzez_email_type( $admin_email, 'admin_featured_submission_listing', $args);

        } else {
            // update_post_meta( $listing_id, 'fave_payment_status', 'paid' );

            // $paid_submission_status    = houzez_option('enable_paid_submission');
            // $listings_admin_approved = houzez_option('listings_admin_approved');

            // if( $listings_admin_approved != 'yes'  && $paid_submission_status == 'per_listing' ){
            //     $post = array(
            //         'ID'            => $listing_id,
            //         'post_status'   => 'publish'
            //     );
            //     $post_id =  wp_update_post($post );
            // } else {
            //     $post = array(
            //         'ID'            => $listing_id,
            //         'post_status'   => 'pending'
            //     );
            //     $post_id =  wp_update_post($post );
            // }


            // if( $is_featured == 1 ) {
            //     update_post_meta( $listing_id, 'fave_featured', 1 );
            //     $invoice_id = houzez_generate_invoice( 'Publish Listing with Featured', 'one_time', $listing_id, $date, $userID, 1, 0, '', $paymentMethod );
            // } else {
            //     $invoice_id = houzez_generate_invoice( 'Listing', 'one_time', $listing_id, $date, $userID, 0, 0, '', $paymentMethod );
            // }
            // update_post_meta( $invoice_id, 'invoice_payment_status', 1 );

            // $args = array(
            //     'listing_title'  =>  get_the_title($listing_id),
            //     'listing_id'     =>  $listing_id,
            //     'invoice_no' =>  $invoice_id,
            // );

            /*
             * Send email
             * */
            houzez_email_type( $user_email, 'paid_submission_listing', $args);
            houzez_email_type( $admin_email, 'admin_paid_submission_listing', $args);
        }

        wp_redirect( $thankyou_page_link ); exit;

    } catch (Conekta_Error $e) {
         //El pago no pudo ser procesado
         $error = '<div class="alert alert-danger">
                <strong>Error!</strong> '.$e->getMessage().'
                </div>';
        print $error;
        

} else if ( isset ($_POST['houzez_conekta_recurring'] ) && $_POST['houzez_conekta_recurring'] == 1 ) {
    /*----------------------------------------------------------------------------------------
    * Payment for Stripe package recuring
    *-----------------------------------------------------------------------------------------*/
    try {
        // $token          =   wp_kses ( esc_html($_POST['stripeToken']) ,$allowed_html);
        // $pack_id        =   intval($_POST['pack_id']);
        // $stripe_plan    =   esc_html(get_post_meta($pack_id, 'fave_package_stripe_id', true));

        // $customer = \Stripe\Customer::create(array(
        //     "email" => $stripeEmail,
        //     "source" => $token,
        //     // "card" =>  $token,
        //     "plan" =>  $stripe_plan
        // ));

        // $stripe_customer_id = $customer->id;
        // $subscription_id = $customer->subscriptions['data'][0]['id'];

        // houzez_save_user_packages_record($userID);
        // if( houzez_check_user_existing_package_status($current_user->ID, $pack_id) ){
        //     houzez_downgrade_package( $current_user->ID, $pack_id );
        //     houzez_update_membership_package($userID, $pack_id);
        // }else{
        //     houzez_update_membership_package($userID, $pack_id);
        // }

        // $invoiceID = houzez_generate_invoice( 'package', 'recurring', $pack_id, $date, $userID, 0, 0, '', $paymentMethod );
        // update_post_meta( $invoiceID, 'invoice_payment_status', 1 );

        // $current_stripe_customer_id =  get_user_meta( $current_user->ID, 'fave_stripe_user_profile', true );
        // $is_stripe_recurring        =   get_user_meta( $current_user->ID, 'houzez_has_stripe_recurring',true );
        // if ($current_stripe_customer_id !=='' && $is_stripe_recurring == 1 ) {
        //     if( $current_stripe_customer_id !== $stripe_customer_id ){
        //         houzez_stripe_cancel_subscription();
        //     }
        // }


        update_user_meta( $current_user->ID, 'fave_stripe_user_profile', $stripe_customer_id );
        update_user_meta( $current_user->ID, 'houzez_stripe_subscription_id', $subscription_id );
        update_user_meta( $current_user->ID, 'houzez_has_stripe_recurring', 1 );

        $args = array();
        houzez_email_type( $user_email,'purchase_activated_pack', $args );

        wp_redirect( $thankyou_page_link ); exit;

    }
    catch (Exception $e) {
        $error = '<div class="alert alert-danger">
                  <strong>Error!</strong> '.$e->getMessage().'
                  </div>';
        print $error;
    }
} else {

    /*----------------------------------------------------------------------------------------
    * Payment for Stripe package
    *-----------------------------------------------------------------------------------------*/
    try {

        // $token  = wp_kses (esc_html($_POST['stripeToken']),$allowed_html);
        // $customer = \Stripe\Customer::create(array(
        //     "email" => $stripeEmail,
        //     "source" => $token // obtained with Stripe.js
        // ));

        // $dash_profile_link = houzez_get_dashboard_profile_link();
        // $userId     =   intval($_POST['userID']);
        // $pay_ammout =   intval($_POST['pay_ammout']);
        // $pack_id    =   intval($_POST['pack_id']);

        // $charge = \Stripe\Charge::create(array(
        //     "amount" => $pay_ammout,
        //     'customer' => $customer->id,
        //     "currency" => $submission_currency,
        //     //"source" => "tok_18Qks9IwlqUqVdUMkzqkPsbV", // obtained with Stripe.js
        //     //"description" => ""
        // ));

        // houzez_save_user_packages_record($userID);
        // if( houzez_check_user_existing_package_status($current_user->ID,$pack_id) ){
        //     houzez_downgrade_package( $current_user->ID, $pack_id );
        //     houzez_update_membership_package($userID, $pack_id);
        // }else{
        //     houzez_update_membership_package($userID, $pack_id);
        // }

        $invoiceID = houzez_generate_invoice( 'package', 'one_time', $pack_id, $date, $userID, 0, 0, '', $paymentMethod );
        update_post_meta( $invoiceID, 'invoice_payment_status', 1 );


        update_user_meta( $current_user->ID, 'houzez_has_stripe_recurring', 0 );

        $args = array();
        houzez_email_type( $user_email,'purchase_activated_pack', $args );

        wp_redirect( $thankyou_page_link ); exit;

    }
    catch (Exception $e) {
        $error = '<div class="alert alert-danger">
                  <strong>Error!</strong> '.$e->getMessage().'
                  </div>';
        print $error;
    }
}

if( !function_exists('houzez_stripe_cancel_subscription') ) {
    function houzez_stripe_cancel_subscription() {
        global $current_user;
        // require_once( get_template_directory() . '/framework/stripe-php/init.php' );

        // $current_user = wp_get_current_user();
        // $userID = $current_user->ID;

        // $stripe_customer_id = get_user_meta($userID, 'fave_stripe_user_profile', true);
        // $subscription_id = get_user_meta($userID, 'houzez_stripe_subscription_id', true);

        // $stripe_secret_key = houzez_option('stripe_secret_key');
        // $stripe_publishable_key = houzez_option('stripe_publishable_key');
        // $stripe = array(
        //     "secret_key"      => $stripe_secret_key,
        //     "publishable_key" => $stripe_publishable_key
        // );
        // \Stripe\Stripe::setApiKey($stripe['secret_key']);

        // $sub = \Stripe\Customer::retrieve($stripe_customer_id);
        // $sub = \Stripe\Subscription::retrieve($subscription_id);
        // $sub->cancel(
        //     array("at_period_end" => true)
        // );
        update_user_meta($current_user->ID, 'houzez_stripe_subscription_id', '');
    }
}
?>