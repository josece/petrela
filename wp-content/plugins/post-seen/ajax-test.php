<?php
/**
 * Plugin Name: Post Seen
 * Plugin URI: http://calleja.mx
 * Description: Cuenta el número de veces que cada uno de las propiedades se ha visto.
 * Version: 1.0.0
 * Author: Jose Calleja
 * Author URI: http://calleja.mx
 * License: GPL2
 */

add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts1' );
function ajax_test_enqueue_scripts1() {
	wp_enqueue_script( 'seen', plugins_url( '/seen.js', __FILE__ ), array('jquery'), '1.0', true );
	wp_localize_script( 'seen', 'agentseen', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));

}

// add_filter( 'the_content', 'agent_seen_display', 99 );
// function agent_seen_display( $content ) {
// 	$love_text = '';

// 	if ( is_single() ) {
		
// 		$seen = get_post_meta( get_the_ID(), 'agent_seen', true );
// 		$seen = ( empty( $seen ) ) ? 0 : $seen;

// 		$seen_text = '<p class="seen-received"><a class="seen-button" href="' . admin_url( 'admin-ajax.php?action=agent_seen_add_click&post_id=' . get_the_ID() ) . '" data-id="' . get_the_ID() . '">give seen</a><span id="seen-count">' . $seen . '</span></p>'; 
	
// 	}

// 	return $content . $love_text;

// }

add_action( 'wp_ajax_nopriv_agent_seen_add_click', 'agent_seen_add_click' );
add_action( 'wp_ajax_agent_seen_add_click', 'agent_seen_add_click' );

function agent_seen_add_click() {
	$seen = get_post_meta( $_REQUEST['post_id'], 'fave_property_agent_seen', true );
	$seen++;
	update_post_meta( $_REQUEST['post_id'], 'fave_property_agent_seen', $seen );
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		// echo $seen;
		die();
	}
	else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}