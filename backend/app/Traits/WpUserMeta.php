<?php

namespace App\Traits;

trait WpUserMeta
{
    public function getNicknameAttribute()
    {
        return $this->meta->where('meta_key', 'nickname')->first()->meta_value;
    }

    public function getFirstNameAttribute()
    {
        return $this->meta->where('meta_key', 'first_name')->first()->meta_value;
    }

    public function getLastNameAttribute()
    {
        return $this->meta->where('meta_key', 'last_name')->first()->meta_value;
    }

    public function getDescriptionAttribute()
    {
        return $this->meta->where('meta_key', 'description')->first()->meta_value;
    }

    public function getRichEditingAttribute()
    {
        return $this->meta->where('meta_key', 'rich_editing')->first()->meta_value;
    }

    public function getCommentShortcutsAttribute()
    {
        return $this->meta->where('meta_key', 'comment_shortcuts')->first()->meta_value;
    }

    public function getAdminColorAttribute()
    {
        return $this->meta->where('meta_key', 'admin_color')->first()->meta_value;
    }

    public function getUseSslAttribute()
    {
        return $this->meta->where('meta_key', 'use_ssl')->first()->meta_value;
    }

    public function getShowAdminBarFrontAttribute()
    {
        return $this->meta->where('meta_key', 'show_admin_bar_front')->first()->meta_value;
    }

    public function getLocaleAttribute()
    {
        return $this->meta->where('meta_key', 'locale')->first()->meta_value;
    }

    public function getWpCapabilitiesAttribute()
    {
        return $this->meta->where('meta_key', 'wp_capabilities')->first()->meta_value;
    }

    public function getWpUserLevelAttribute()
    {
        return $this->meta->where('meta_key', 'wp_user_level')->first()->meta_value;
    }

    public function getDismissedWpPointersAttribute()
    {
        return $this->meta->where('meta_key', 'dismissed_wp_pointers')->first()->meta_value;
    }

    public function getFaveAuthorTitleAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_title')->first()->meta_value;
    }

    public function getFaveAuthorCompanyAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_company')->first()->meta_value;
    }

    public function getFaveAuthorPhoneAttribute()
    {
        try {
            $phone = $this->meta->where('meta_key', 'fave_author_phone')->first()->meta_value;
        } catch (\Exception $e) {
            return '';
        }
        return $phone;
    }

    public function getFaveAuthorFaxAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_fax')->first()->meta_value;
    }

    public function getFaveAuthorMobileAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_mobile')->first()->meta_value;
    }

    public function getFaveAuthorSkypeAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_skype')->first()->meta_value;
    }

    public function getFaveAuthorCustomPictureAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_custom_picture')->first()->meta_value;
    }

    public function getFaveAuthorAgentIdAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_agent_id')->first()->meta_value;
    }

    public function getPackageIdAttribute()
    {
        return $this->meta->where('meta_key', 'package_id')->first()->meta_value;
    }

    public function getPackageActivationAttribute()
    {
        return $this->meta->where('meta_key', 'package_activation')->first()->meta_value;
    }

    public function getPackageListingsAttribute()
    {
        return $this->meta->where('meta_key', 'package_listings')->first()->meta_value;
    }

    public function getPackageFeaturedListingsAttribute()
    {
        return $this->meta->where('meta_key', 'package_featured_listings')->first()->meta_value;
    }

    public function getFavePaypalProfileAttribute()
    {
        return $this->meta->where('meta_key', 'fave_paypal_profile')->first()->meta_value;
    }

    public function getFaveStripeUserProfileAttribute()
    {
        return $this->meta->where('meta_key', 'fave_stripe_user_profile')->first()->meta_value;
    }

    public function getFaveAuthorFacebookAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_facebook')->first()->meta_value;
    }

    public function getFaveAuthorLinkedinAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_linkedin')->first()->meta_value;
    }

    public function getFaveAuthorTwitterAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_twitter')->first()->meta_value;
    }

    public function getFaveAuthorPinterestAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_pinterest')->first()->meta_value;
    }

    public function getFaveAuthorInstagramAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_instagram')->first()->meta_value;
    }

    public function getFaveAuthorYoutubeAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_youtube')->first()->meta_value;
    }

    public function getFaveAuthorVimeoAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_vimeo')->first()->meta_value;
    }

    public function getFaveAuthorGoogleplusAttribute()
    {
        return $this->meta->where('meta_key', 'fave_author_googleplus')->first()->meta_value;
    }

    public function getSessionTokensAttribute()
    {
        return $this->meta->where('meta_key', 'session_tokens')->first()->meta_value;
    }

    public function getUserHadFreePackageAttribute()
    {
        return $this->meta->where('meta_key', 'user_had_free_package')->first()->meta_value;
    }

    public function getFullNameAttribute()
    {
        return trim("{$this->first_name} {$this->last_name}");
    }
}
