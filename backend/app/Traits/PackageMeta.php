<?php

namespace App\Traits;

trait PackageMeta
{
    public function getFaveBillingTimeUnitAttribute()
    {
        return $this->meta->where('meta_key', 'fave_billing_time_unit')->first()->meta_value;
    }

    public function getFaveBillingUnitAttribute()
    {
        return $this->meta->where('meta_key', 'fave_billing_unit')->first()->meta_value;
    }

    public function getFavePackageFeaturedListingsAttribute()
    {
        return $this->meta->where('meta_key', 'fave_package_featured_listings')->first()->meta_value;
    }

    public function getFavePackageListingsAttribute()
    {
        return $this->meta->where('meta_key', 'fave_package_listings')->first()->meta_value;
    }

    public function getFavePackagePopularAttribute()
    {
        return $this->meta->where('meta_key', 'fave_package_popular')->first()->meta_value;
    }

    public function getFavePackagePriceAttribute()
    {
        return $this->meta->where('meta_key', 'fave_package_price')->first()->meta_value;
    }

    public function getFavePackageVisibleAttribute()
    {
        return $this->meta->where('meta_key', 'fave_package_visible')->first()->meta_value;
    }

    public function getFaveUnlimitedListingsAttribute()
    {
        return $this->meta->where('meta_key', 'fave_unlimited_listings')->first()->meta_value;
    }

    public function getSlideTemplateAttribute()
    {
        return $this->meta->where('meta_key', 'slide_template')->first()->meta_value;
    }

}
