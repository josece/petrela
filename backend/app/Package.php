<?php

namespace App;

use App\Scopes\PackageScope;
use App\Traits\PackageMeta;

class Package extends Post
{
    use PackageMeta;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PackageScope);
    }
}
