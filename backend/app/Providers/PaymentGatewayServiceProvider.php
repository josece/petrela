<?php

namespace App\Providers;

use Conekta\Conekta;
use Illuminate\Support\ServiceProvider;

class PaymentGatewayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        Conekta::setApiKey($this->app->config->get('services.conekta.private'));
        Conekta::setLocale('es');
    }
}
