<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProcessPaymentWithOxxoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_id' => [
                'bail',
                'required',
                Rule::exists('wp_posts', 'ID')->where(function ($query) {
                    $query->where('post_type', 'houzez_packages')
                        ->where('post_status', 'publish');
                }),
            ],
            'user_id' => 'required',
            'invoice_id' => 'required',
        ];
    }
}
