<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuthenticationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->has('key') || $request->key != env('PETRELA_API_KEY')) {
            abort(401);
        }

        return $next($request);
    }
}
