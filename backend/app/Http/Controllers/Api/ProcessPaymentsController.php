<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProcessPaymentWithCardRequest;
use App\Http\Requests\ProcessPaymentWithOxxoRequest;
use App\Package;
use App\WpUser;
use Conekta\Charge;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Support\Facades\Log;

class ProcessPaymentsController extends AbstractApiController
{
    public function card(ProcessPaymentWithCardRequest $request)
    {
        $package = Package::with('meta')->findOrFail(Binput::get('package_id'));
        $user = WpUser::with('meta')->findOrFail(Binput::get('user_id'));

        try {
            $charge = Charge::create($this->prepareChargeData([
                'card' => Binput::get('card_token'),
            ]));
        } catch (\Exception $e) {
            Log::info($e);
            return $this->noSuccess();
        }

        return $this->success();
    }

    public function oxxo(ProcessPaymentWithOxxoRequest $request)
    {
        try {
            $charge = Charge::create($this->prepareChargeData([
                'cash' => [
                    'type' => 'oxxo',
                ],
            ]));
        } catch (\Exception $e) {
            Log::info($e);
            return $this->noSuccess();
        }

        return $this->success([
            'barcode' => $charge->payment_method->barcode,
            'barcodeUrl' => $charge->payment_method->barcode_url,
            'object' => $charge->payment_method->object,
            'type' => $charge->payment_method->type,
            'expiresAt' => $charge->payment_method->expires_at,
            'storeName' => $charge->payment_method->store_name,
        ]);
    }

    private function prepareChargeData($params = [])
    {
        $package = Package::with('meta')->findOrFail(Binput::get('package_id'));
        $user = WpUser::with('meta')->findOrFail(Binput::get('user_id'));
        $invoiceId = Binput::get('invoice_id');

        return array_merge([
            'description' => 'Petrela Package',
            'reference_id' => "{$user->ID}_{$package->ID}_{$invoiceId}_".time(),
            'amount' => $package->fave_package_price * 100,
            'currency' => 'MXN',
            'details' => [
                'name' => $user->full_name ?: $user->ID,
                'phone' => $user->fave_author_phone ?: "123-456-789",
                'email' => $user->user_email ?: $user->ID,
                'line_items' => [
                    [
                        'name' => $package->post_title,
                        'description' => 'Paquete: '.$package->post_title,
                        'unit_price' => $package->fave_package_price,
                        'quantity' => 1,
                        'sku' => $package->ID,
                        'type' => 'package',
                    ],
                ],
            ],
        ], $params);
    }
}
