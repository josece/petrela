<?php

namespace App\Http\Controllers\Api;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebHookController extends AbstractApiController
{
    public function handleWebhook(Request $event)
    {
        $charge = $event->data['object'];

        if ($event->type == 'charge.paid') {
            if ($charge['payment_method']['type'] == 'oxxo') {
                $reference = explode('_', $charge['reference_id']);
                $reference = [
                    'userId' => $reference[0],
                    'packageId' => $reference[1],
                    'invoiceId' => $reference[2],
                    'time' => $reference[3],
                ];

                // If the app that is going to be connected to this gateway
                // has a webhook url, let's send the info to it's service.
                if (!empty(env('WEBHOOK_URL'))) {
                    $client = new Client([
                        'timeout' => 3600.0,
                    ]);

                    try {
                        $client->request('POST', env('WEBHOOK_URL'), [
                            'form_params' => [
                                'package_id' => $reference['packageId'],
                                'payment_gateway' => env('PETRELA_API_KEY'),
                                'user_id' => $reference['userId'],
                                'invoice_id' => $reference['invoiceId'],
                            ],
                        ]);
                    } catch (\Exception $e) {
                        Log::info($e);
                    }
                }

                // Log for convenience.
                Log::info("charge.paid");
                Log::info($event->all());
                Log::info("charge.paid");
            }
        }

        return $this->success();
    }
}
