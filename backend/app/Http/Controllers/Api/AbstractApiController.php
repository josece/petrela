<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;

abstract class AbstractApiController extends Controller
{
    /**
     * The HTTP response headers.
     *
     * @var array
     */
    protected $headers = [];

    /**
     * The HTTP response data.
     *
     * @var mixed
     */
    protected $data = null;

    /**
     * The HTTP response status code.
     *
     * @var int
     */
    protected $statusCode = 200;

    /**
     * Set the response headers.
     *
     * @param array $headers
     *
     * @return $this
     */
    protected function setHeaders(array $headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Set the response meta data.
     *
     * @param array $data
     *
     * @return $this
     */
    protected function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Set the response status code.
     *
     * @param int $statusCode
     *
     * @return $this
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Respond with a successful response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function success($data = ['status' => 'complete'])
    {
        return $this->setStatusCode(200)->setData($data)->respond();
    }

    /**
     * Respond with a no successful response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function noSuccess()
    {
        $data = [
            'status' => 'fail',
        ];

        return $this->setStatusCode(402)->setData($data)->respond();
    }

    /**
     * Respond with a no content response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function noContent()
    {
        return $this->setStatusCode(204)->respond();
    }

    /**
     * Build the response.
     *
     * @return \Illuminate\Http\Response
     */
    protected function respond()
    {
        $response = [];

        if (!is_null($this->data)) {
            $response = $this->data;
        }

        return Response::json($response, $this->statusCode, $this->headers);
    }
}
