<?php

namespace App;

use App\Traits\WpUserMeta;
use Illuminate\Database\Eloquent\Model;

class WpUser extends Model
{
    use WpUserMeta;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wp_users';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    public function meta()
    {
        return $this->hasMany(UserMeta::class, 'user_id');
    }
}
