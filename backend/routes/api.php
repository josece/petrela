<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group([
    'namespace' => 'Api',
    'prefix' => 'v1',
], function () {
    Route::group([
        'middleware' => 'auth.api',
    ], function () {
        // Route to process card payments
        Route::post('payments/card/process', [
            'as' => 'payments.card.process',
            'uses' => 'ProcessPaymentsController@card',
        ]);

        // Route to process oxxo payments
        Route::post('payments/oxxo/process', [
            'as' => 'payments.oxxo.process',
            'uses' => 'ProcessPaymentsController@oxxo',
        ]);
    });

    // Route to process conekta webhooks
    Route::post('webhook/6qs7It7iQTymiFR5xLTeTL3ankv007U3L7JdTPm7naoD0La622HSryj5O2LApVcG', [
        'as' => 'webhook',
        'uses' => 'WebHookController@handleWebhook',
    ]);
});
